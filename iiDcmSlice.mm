/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.

 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

// -- hack to quiet warning messages in Xcode for now     --
// -- https://www.bignerdranch.com/blog/a-bit-on-warnings --

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#import "iiDicom.h"

// -- http://stackoverflow.com/questions/31665095/xcode-error-compiling-c-expected-member-name-or-after-declaration-specifie --
// -- fixed in iiDicom_Prefix.pch                                                                                            --

// #undef verify	// -- AssertMacros.h also defines verify.  Need this to stop tons of compilation errors

#pragma clang diagnostic pop

@implementation iiDcmSlice

// -- full paths to the generic ICC profiles --

// #define  kGenericRGBProfilePathStr "/System/Library/ColorSync/Profiles/Generic RGB Profile.icc"
// #define kGenericGrayProfilePathStr "/System/Library/ColorSync/Profiles/Generic Gray Profile.icc"

#pragma mark - Initializers
#pragma mark -

- (instancetype) init           //Provided in case initWithDicomFileSlice isn't called instead
{
	DLog (@"iiDcmSlice: Use initWithDicomFileSlice instead of init to instantiate object");
	return [self initWithDicomFileSlice: @"unknown.dcm"];
}

// -- Registers the filename provided, storing it in the dcmFilename. - Adds "Dicom Filename" key to the dictionary. --

- (instancetype) initWithDicomFileSlice: (NSString *) dicomFilename
{
	if (!(self = [super init])) return nil;
	
	  _dicomFilename = dicomFilename;
    _dicomDictionary = [[iiDcmDictionary alloc] initWithDicomFile: _dicomFilename];         // get the dicom dictionary
			
	return self;
}

#pragma mark - Other Methods
#pragma mark -

// -- Returns the slice number looked up from the dictionary, used primarily for sorting into slice order. --

- (NSNumber *) sliceLocation
{
	NSString *num = [_dicomDictionary metaDataSetValueByKey: @"SliceLocation"];

    if (num)
        then return @(num.floatValue);
        else return @-1.0F;
}

// -- Returns the slice instance number looked up from the dictionary, used for secondary sorting into slice order. --

- (NSNumber *) sliceInstanceNumber
{
	NSString *num = [_dicomDictionary metaDataSetValueByKey: @"InstanceNumber"];

    if (num)
        then return @(num.intValue);
        else return @-1;
}

// -- creates a data provider with the dicom image data --

CGDataProviderRef createDicomDataProvider(void *data, size_t dataSize)
{
	CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, data, dataSize, NULL);
	
	return dataProvider;
}

// -- gets the slice from a file and stores it in _imageData - Returns YES is everything is okay, otherwise NO. --

- (BOOL) getDicomImageData: (BOOL) autoWindowLevel
{
	if (_imageData) return YES;
	
@autoreleasepool {
    
	   DicomImage  *dcmImage = new DicomImage(_dicomFilename.UTF8String);
    DcmFileFormat fileformat;
    
    if (dcmImage != NULL)
	{
		if (dcmImage->getStatus( ) == EIS_Normal)
		{
                               _planes = 3;     // color dicom image
                _bitsPerPixelComponent = 8;     // color dicom image

            if (dcmImage->isMonochrome( ))
			{
                               _planes = 1;     // monochrome dicom image
				_bitsPerPixelComponent = 16;    // monochrome dicom image
			}
            
                  _width = (int) dcmImage->getWidth( );
                 _height = (int) dcmImage->getHeight( );
			_bytesPerRow = (int) _width * (_bitsPerPixelComponent / 8) * _planes;

			if (autoWindowLevel) then dcmImage->setMinMaxWindow( );
			
			if (_planes == 1)
			{
				dcmImage->getMinMaxValues(        _monochromeMinValue,         _monochromeMaxValue, 0);
				dcmImage->getMinMaxValues(_monochromeMinPossibleValue, _monochromeMaxPossibleValue, 1);
			}
			else
			{
                        _monochromeMinValue = (double)   0;
                        _monochromeMaxValue = (double) 255;
				_monochromeMinPossibleValue = (double)   0;
				_monochromeMaxPossibleValue = (double) 255;
			}
			
// -- allocate buffer to hold data - we allocate this way to pass the analyzer --
            
			size_t imageDataSize = (size_t) _height * (size_t) _bytesPerRow;
            size_t memToAllocate = imageDataSize * sizeof(unsigned char);
            
            if (memToAllocate > 0) then _imageData = (unsigned char *) malloc(memToAllocate);
            
			if (_imageData == NULL)
			{
				delete dcmImage;
				return NO;
			}

// -- safety check to make sure we're not going to overflow the buffer --
            
			if ((size_t) dcmImage->getOutputDataSize(_bitsPerPixelComponent) != imageDataSize)
			{
				DLog (@"iiDcmSlice: getDicomImageData: mismatch in buffer sizes");
				free(_imageData);
				_imageData = NULL;
				delete dcmImage;
				return NO;
			}
			
			Uint8 *pixelData = (Uint8 *) dcmImage->getOutputData(_bitsPerPixelComponent);
						
			if (pixelData == NULL)
			{
				DLog (@"iiDcmSlice: getDicomImageData: DICOM File pixelData is empty");
				free(_imageData);
				_imageData = NULL;
				delete dcmImage;
				return NO;
			}
			
			bcopy ((void *) pixelData, (void *) _imageData, imageDataSize);          // copy image data
			delete dcmImage;
		}
		else
		{
			DLog (@"iiDcmSlice: getDicomImageData: Cannot load DICOM image %@ (%s)", _dicomFilename, DicomImage::getString(dcmImage->getStatus( )));
			delete dcmImage;
			return NO;
		}
	}
	else
	{
		DLog (@"iiDcmSlice: getDicomImageData: DicomImage returned NULL for %@", _dicomFilename);
		return NO;
	}
    
}       // autoreleasepool ends here
	
	return YES;
}

// Gets the slice from a file and stores it in _cgrDicomSlice as a CGImageRef
// NULL is stored as the dicomSlice in case of error
// Dynamically loads the dicom slice, and returns as a CGImageRef - Returns nil in case of error

- (CGImageRef) sliceAsCGImageRef: (BOOL) autoWindowLevel
{
	if (_cgirDicomSlice) return _cgirDicomSlice;
	
	if (! [self getDicomImageData: autoWindowLevel]) return nil;
				
// -- setup the data provider --

               size_t imageDataSize = (size_t) _height * (size_t) _bytesPerRow;
	CGDataProviderRef  dataProvider = createDicomDataProvider((void *) _imageData, imageDataSize);
    
	if (dataProvider == NULL)
	{
		DLog (@"iiDcmSlice: sliceASCGImageRef: couldn't create DICOM data provider");
		return nil;
	}
			
// -- create the colorspace --
    
	CGBitmapInfo bitmapInfo;
    
// -- 3 planes is color - otherwise it's a monochrome image --
    
    if (_planes == 3)
	{
        cgirGenericColorSpace = CGColorSpaceCreateDeviceRGB( );
		bitmapInfo = 0;                         // no need to change endianism on color images - they are 8 bit
	}
	else
	{
        cgirGenericColorSpace = CGColorSpaceCreateDeviceGray( );
		bitmapInfo = kCGBitmapByteOrder16Host;	// macro that changes endian depending on architecture - swaps unsigned int 16's byte order
	}
    
// -- http://stackoverflow.com/questions/10298165/error-cgimagecreate-invalid-image-bits-pixel-8 - create the CGImageRef --
			
	_cgirDicomSlice = CGImageCreate((size_t)  _width,
                                    (size_t) _height,
                                    (size_t)  _bitsPerPixelComponent,
                                    (size_t) (_bitsPerPixelComponent * _planes),
                                    (size_t) _bytesPerRow,
                                    cgirGenericColorSpace,
                                    kCGImageAlphaNone | bitmapInfo,
                                    dataProvider,
                                    NULL,
                                    true,
                                    kCGRenderingIntentDefault);
			
	CGDataProviderRelease (dataProvider);           // release the data provider
	
	if (_cgirDicomSlice == NULL)
	{
		DLog (@"iiDcmSlice: sliceAsCGImageRef: Couldn't create CGImageRef for image data!");
		return nil;
	}
	
	return _cgirDicomSlice;
}

// -- gets the slice from a file and stores it in _nsiDicomSlice as a NSImage (on Mac) or as a UIImage (on iOS)                               --
// -- dynamically loads the dicom slice - returns an NSImage (on Mac) or returns an UIImage (on iOS)                                          --
// -- NULL is stored as the dicomSlice in case of error                                                                                       --
// -- context specifies the NSGraphicsContext to draw in - if you specify nil for this, it will default to [NSGraphicsContext currentContext] --
// -- NSImage no longer set with setScalesWhenResized = YES - Returns nil in case of error                                                    --

#ifdef __APPLE__

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

- (UIImage *) sliceAsUIImage: (BOOL) autoWindowLevel context: (CGContextRef) context
{
	CGContextRef gcontext;

// -- on iOS, _nsiDicomSlice is a UIImage pointer --
    
	if (_nsiDicomSlice) return _nsiDicomSlice;
	
	if (![self sliceAsCGImageRef: autoWindowLevel]) return nil;
		
	CGRect imageRect = CGRectMake(0.0, 0.0, (float) _width, (float) _height);       // figure out image size
	DLog (@"iiDicom: sliceAsUIImage: Rect: %@", NSStringFromCGRect(imageRect));

// -- create the image --
    
    _nsiDicomSlice = [[UIImage alloc] init];        // on iOS, _nsiDicomSLize is a UIImage pointer
    
	if (_nsiDicomSlice == nil)
	{
		DLog (@"iiDcmSlice: sliceAsUIImage : Couldn't create UIImage for image data!");
		return nil;
	}
		
// -- draw the bitmap (CGImage) into the NSImage --
    
//	[_nsiDicomSlice lockFocus];         // Leaking! - doesn't work on iOS

    if (context)
        then gcontext = context;
        else gcontext = UIGraphicsGetCurrentContext( );
    
//  CGContextRef imageContext = (CGContextRef) gcontext.graphicsPort;
//
//	CGContextDrawImage (imageContext, * (CGRect *) &imageRect, _cgirDicomSlice);
	CGContextDrawImage (gcontext, imageRect, _cgirDicomSlice);

// -- https://github.com/MiMo42/MMTabBarView/pull/22 - to comment out setScalesWhenResized method --
    
//  [_nsiDicomSlice          unlockFocus     ];         // doesn't work on iOS
//	[_nsiDicomSlice setScalesWhenResized: YES];         // automatically rescale images to fit
	
	return _nsiDicomSlice;	
}

#elif TARGET_OS_MAC

- (NSImage *) sliceAsNSImage: (BOOL) autoWindowLevel context: (NSGraphicsContext *) context
{
	NSGraphicsContext *gcontext;

// -- on the Mac, _nsiDicomSlice is an NSImage pointer --
    
    if (_nsiDicomSlice) return _nsiDicomSlice;
	
	if (![self sliceAsCGImageRef: autoWindowLevel]) return nil;
		
	NSRect imageRect = NSMakeRect(0.0, 0.0, (float) _width, (float) _height);       // figure out image size
	DLog (@"iiDicom: sliceAsNSImage: Rect: %@", NSStringFromRect(imageRect));

// -- create the image --
    
    _nsiDicomSlice = [[NSImage alloc] initWithSize: imageRect.size];        // on the Mac, _nsiDicomSlice is an NSImage pointer

	if (_nsiDicomSlice == nil)
	{
		DLog (@"iiDcmSlice: sliceAsNSImage : Couldn't create NSImage for image data!");
		return nil;
	}
		
// -- draw the bitmap (CGImage) into the NSImage --

    [_nsiDicomSlice lockFocus];                         // Leaking! -- works on Mac

    if (context)
        then gcontext = context;
        else gcontext = [NSGraphicsContext currentContext];

    CGContextRef imageContext = (CGContextRef) gcontext.graphicsPort;
    
	CGContextDrawImage (imageContext, * (CGRect *) &imageRect, _cgirDicomSlice);

// -- https://github.com/MiMo42/MMTabBarView/pull/22 - to comment out setScalesWhenResized method --
    
    [_nsiDicomSlice          unlockFocus     ];         // works on Mac
//	[_nsiDicomSlice setScalesWhenResized: YES];         // automatically rescale images to fit
	
	return _nsiDicomSlice;	
}

#endif

#endif

// -- returns nil on error --

- (NSData *) sliceAsRawData: (BOOL) autoWindowLevel
{
	if (![self getDicomImageData: autoWindowLevel]) return nil;
    
	return [NSData dataWithBytesNoCopy: (void *) _imageData
								length: (unsigned) _height * (unsigned) _bytesPerRow
						  freeWhenDone: NO];
}

// -- Returns the slice scaled to the units specified in the file.        --
// -- If the file is CT, then this will be hounsfield units.              --
// -- If the slice cannot be loaded or is not monochrome nil is returned. --

- (NSData *) sliceAsRawScaledData
{
	if (_imageDataScaled) return [NSData dataWithBytesNoCopy: (void *) _imageDataScaled
                                                      length: (unsigned) _height * (unsigned) _width * (unsigned) sizeof(float)
                                                freeWhenDone: NO];
		
	if ((! [self getDicomImageData: NO]) || (! [self isMonochrome])) return nil;      // do some sanity checks
	
//  -- If we go 64 bit, this might need changing. --
    
	if (sizeof(unsigned short) != 2)
	{
		DLog (@"iiDcmSlice: sliceAsRawScaledData: unsigned short != 2 bytes");
		return nil;
	}
	
	if ((_imageDataScaled = (float *) malloc((size_t) _width * (size_t) _height * sizeof(float))) == NULL) return NULL;

             float *imageDataScaledPtr = _imageDataScaled;
	unsigned short       *imageDataPtr = (unsigned short *) _imageData;
             float    minPossibleValue = (float) [self monochromeMinPossibleValue];
             float    maxPossibleValue = (float) [self monochromeMaxPossibleValue];

// -- _imageData with autowindowlevel: NO is MinPossibleValue..MaxPossibleValue scaled to the range 0..65535 --
// -- we need to scale to the actual raw value                                                               --
    
	for (int i = 0; i < (_width * _height); i++, imageDataScaledPtr++, imageDataPtr++)
		*imageDataScaledPtr = ((float) (*imageDataPtr)) * ((maxPossibleValue - minPossibleValue) / 65535.0F) + minPossibleValue;

	return [NSData dataWithBytesNoCopy: (void *) _imageDataScaled
								length: (unsigned) _height * (unsigned) _width * (unsigned) sizeof(float)
						  freeWhenDone: NO];
}

// -- deprecated --

-          (NSData *) sliceAsHounsfield { return [self sliceAsRawScaledData]; }
- (iiDcmDictionary *)   dicomDictionary { return _dicomDictionary;            }

//  -- Returns YES if the slice is monochrome, otherwise NO for color or if the slice hasn't been read. --

- (BOOL) isMonochrome
{
	if (_planes == 1)
        then return YES;
        else return  NO;
}

// -- returns YES if the slice is hounsfield capable - http://www.dfanning.com/fileio_tips/hounsfield.html --

- (BOOL) isHounsfield
{
	if (_dicomDictionary)
	{
		// -- If we have RescaleSlope and RescaleIntercept in the dictionary, it's hounsfield. --
        
		NSString *rescaleSlopeStr	  = [_dicomDictionary metaDataSetValueByKey:     @"RescaleSlope"];
		NSString *rescaleInterceptStr = [_dicomDictionary metaDataSetValueByKey: @"RescaleIntercept"];
		
		if ((!rescaleSlopeStr) || (!rescaleInterceptStr))
            then return  NO;
            else return YES;
	}
    
	return NO;
}

// -- returns the volume of the voxels in units cubed (e.g. mm3) - In case of error, -1.0 is returned. --

- (float) voxelVolume
{
	float voxelVolume = -1.0F;
    
	NSString *pixelSpacing		   = [[self dicomDictionary] metaDataSetValueByKey:         @"PixelSpacing"];
	NSString *spacingBetweenSlices = [[self dicomDictionary] metaDataSetValueByKey: @"SpacingBetweenSlices"];
	
	if (pixelSpacing && spacingBetweenSlices)
	{
		// -- figure out the voxel volume in units cubed --
        
		NSArray *components = [pixelSpacing componentsSeparatedByString: @"\\"];
        
		if (components && (components.count == 2))
		{
			float yCm = [components[0] floatValue];
			float xCm = [components[1] floatValue];
			float zCm = spacingBetweenSlices.floatValue;
		
			voxelVolume = xCm * yCm * zCm;
		}
	}

	return voxelVolume;
}

// -- metrics accessor methods --

- (int) planes							{ return                     _planes; }
- (int) width							{ return                      _width; }
- (int) height							{ return                     _height; }
- (int) bitsPerPixelComponent			{ return      _bitsPerPixelComponent; }
- (int) bytesPerRow						{ return                _bytesPerRow; }

- (double) monochromeMinValue			{ return         _monochromeMinValue; }
- (double) monochromeMaxValue			{ return         _monochromeMaxValue; }
- (double) monochromeMinPossibleValue	{ return _monochromeMinPossibleValue; }
- (double) monochromeMaxPossibleValue	{ return _monochromeMaxPossibleValue; }

- (NSString *) filename					{ return              _dicomFilename; }

#pragma mark - Memory Management
#pragma mark -

- (void) dealloc
{
	DLog (@"iiDcmSlice: dealloc");
	
	if (      _imageData) free (      _imageData);
	if (_imageDataScaled) free (_imageDataScaled);
    
	if (      _cgirDicomSlice)      CGImageRelease (      _cgirDicomSlice);
	if (cgirGenericColorSpace) CGColorSpaceRelease (cgirGenericColorSpace);
}

@end
