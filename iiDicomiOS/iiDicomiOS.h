//
//  iiDicomiOS.h - iiDicomiOS - Created by Robert Russes on 14-Mar-16
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT              double   iiDicom_iOSVersionNumber;   //! Project version number for iiDicomiOS
FOUNDATION_EXPORT const unsigned char iiDicom_iOSVersionString[];   //! Project version string for iiDicomiOS

// -- In this header, you should import all the public headers of your framework using statements like #import <iiDicomiOS/PublicHeader.h>. --

#import         <iiDicom.h>
#import <iiDcmDictionary.h>
#import      <iiDcmSlice.h>
#import     <iiDcmSeries.h>
#import    <iiDcmFileset.h>
