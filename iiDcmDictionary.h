/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.
 
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

@interface iiDcmDictionary : NSObject
{
	NSMutableDictionary           *_metaInformation;
	NSMutableDictionary               *_metaDataSet;
	NSMutableDictionary *_metaForImageKitProperties;
               NSString               *_dcmFilename;
                   BOOL                    fileRead;
}

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *metaInformation;		// returns Information dictionary - may return nil
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary     *metaDataSet;		// returns     DataSet dictionary - may return nil
@property (NS_NONATOMIC_IOSONLY, readonly, copy)     NSString   *dicomFilename;		// returns dicom filename as provided to initWithDicomFile.
@property (NS_NONATOMIC_IOSONLY, readonly, copy)     NSString       *seriesUid;		// identifier which uniquely identifies the series this slice belongs to

// -- convenience methods - may return nil --

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *metaDictionaryAllForImageKitProperties;

- (instancetype)                         init;                                                          // DO NOT USE
- (instancetype)			initWithDicomFile: (NSString *) dicomFilename NS_DESIGNATED_INITIALIZER;	// use this to instantiate
- (  NSString *)        metaDataSetValueByKey: (NSString *) key;
- (  NSString *)	metaInformationValueByKey: (NSString *) key;
- (  NSNumber *)             findAndGetUint16: ( NSArray *) tagPair;        // for use with Swift
- (  NSNumber *)             findAndGetSint32: ( NSArray *) tagPair;        // for use with Swift
- (        BOOL)           tagExistsWithValue: ( NSArray *) tagPair;        // for use with Swift
- (        BOOL)                    tagExists: ( NSArray *) tagPair;        // for use with Swift
- (  NSString *)           findAndGetOFString: ( NSArray *) tagPair;        // for use with Swift
- (  NSNumber *)            findAndGetFloat32: ( NSArray *) tagPair;        // for use with Swift
- (  NSNumber *)            findAndGetFloat64: ( NSArray *) tagPair;        // for use with Swift

// - (NSMutableArray *) findAndGetFloat32Array: (NSArray *) tagPair;
// - (NSMutableArray *) findAndGetFloat64Array: (NSArray *) tagPair;

// iiDcmDictionary *dicomDictionary = [[iiDcmDictionary alloc] initWithDicomFile: __DICOM_FILENAME__];
//    NSDictionary         *dataSet = [dicomDictionary metaDataSet];
//
// if (dataSet)
//      then DLog (@"Dicom Dataset Dictionary: %@", dataSet);
//      else DLog (@"Dicom Dictionary Not Found");
//
// [dicomDictionary release];           // not required under ARC

@end
