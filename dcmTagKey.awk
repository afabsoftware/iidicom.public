# -- used to generate dcmTagKey.swift                                                      --
# -- run this script against DCMTK's dcdeftag.h file (located under the dcmdata directory) --
# -- awk -f dcmTagkey.awk < dcdeftag.h > dcmTagKey.swift                                   --

{
    if ($1 == "#define") {
        if (substr($2,1,4) == "DCM_") {
            if (substr($3,1,9) == "DcmTagKey") {
#               print "let " $2 " = DcmTagKey(group: " substr($3,11,16) " element: " substr($4,1,6) ")"
                print "let " $2 ": [UInt] = [" substr($3,11,16) " " substr($4,1,6) "]"
            }
        }
    }

}
