//
//  iiDicommacOS.h - iiDicommacOS - Created by Robert Russes on 14-Mar-16
//

#import <Cocoa/Cocoa.h>

FOUNDATION_EXPORT              double   iiDicom_macOSVersionNumber;       //! Project version number for iiDicommacOS
FOUNDATION_EXPORT const unsigned char iiDicom_macOSVersionString[];       //! Project version string for iiDicommacOS

// -- In this header, you should import all the public headers of your framework using statements like #import <iiDicommacOS/PublicHeader.h>. --

#import         <iiDicom.h>
#import <iiDcmDictionary.h>
#import      <iiDcmSlice.h>
#import     <iiDcmSeries.h>
#import    <iiDcmFileset.h>
