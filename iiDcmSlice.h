/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

@class iiDcmDictionary;

@interface iiDcmSlice : NSObject
{
           NSString       *_dicomFilename;	// filename containing the slice
	iiDcmDictionary     *_dicomDictionary;	// Dicom dictionary for the slice
         CGImageRef       _cgirDicomSlice;	// CGImageRef containing the slice image data
	CGColorSpaceRef	cgirGenericColorSpace;  // color space associated with the above CGImageRef
      unsigned char           *_imageData;	// pointer to the image data
              float     *_imageDataScaled;	// pointer to the image data in scaled (hounsfield if CT) units

#ifdef __APPLE__
    #if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
        UIImage	*_nsiDicomSlice;            // UIImage containing the slice image data - need UIImage for iOS
    #elif TARGET_OS_MAC
        NSImage	*_nsiDicomSlice;            // NSImage containing the slice image data - for Mac
    #endif
#endif

// -- image metrics --
    
       int                      _planes;	// number of components per pixel / 1 = monochrome - 3 = color
	   int                       _width;	//  width of image in pixels
	   int                      _height;	// height of image in pixels
	   int		 _bitsPerPixelComponent;	// number of bits per pixel component
	   int                 _bytesPerRow;	// number of bytes in a row
	double			_monochromeMinValue;	// minimum value of the data (monochrome only)
	double			_monochromeMaxValue;	// maximum value of the data (monochrome only)
	double	_monochromeMinPossibleValue;	// minimum possible value of the data (monochrome only)
	double	_monochromeMaxPossibleValue;	// maximum possible value of the data (monochrome only)
}

// -- for now, setup sliceAsUIImage method on iOS --

#ifdef __APPLE__
    #if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
        - (UIImage *) sliceAsUIImage: (BOOL) autoWindowLevel context: (CGContextRef) context;           // returns the slice as a UIImage (iOS)
    #elif TARGET_OS_MAC
        - (NSImage *) sliceAsNSImage: (BOOL) autoWindowLevel context: (NSGraphicsContext *) context;	// returns the slice as a NSImage (Mac)
    #endif
#endif

- (instancetype)                   init;                                                        // DO NOT USE
- (instancetype) initWithDicomFileSlice: (NSString *) dicomFilename NS_DESIGNATED_INITIALIZER;	// use this to instantiate slice

- (CGImageRef) sliceAsCGImageRef: (BOOL) autoWindowLevel CF_RETURNS_NOT_RETAINED;           // returns the slice as a CGImageRef
- (  NSData *)	  sliceAsRawData: (BOOL) autoWindowLevel;                                   // returns the slice as raw data

@property (NS_NONATOMIC_IOSONLY, readonly,   copy)        NSString             *filename;
@property (NS_NONATOMIC_IOSONLY, readonly,   copy)        NSNumber        *sliceLocation;	// returns the slice location looked up from the dictionary
@property (NS_NONATOMIC_IOSONLY, readonly,   copy)        NSNumber  *sliceInstanceNumber;	// returns the slice instance number looked up from the dictionary
@property (NS_NONATOMIC_IOSONLY, readonly,   copy)          NSData *sliceAsRawScaledData;	// returns the slice in units scaled to hounsfield or as specified in the file (floats)
@property (NS_NONATOMIC_IOSONLY, readonly,   copy)          NSData    *sliceAsHounsfield;	// deprecated - use sliceAsRawScaledData
@property (NS_NONATOMIC_IOSONLY, readonly, strong) iiDcmDictionary      *dicomDictionary;	// returns the dicom dictionary

@property (NS_NONATOMIC_IOSONLY, getter=isMonochrome, readonly)  BOOL  monochrome;		// YES if the slice is mono - must be called after a sliceAs command
@property (NS_NONATOMIC_IOSONLY, getter=isHounsfield, readonly)  BOOL  hounsfield;		// YES if the slice is hounsefield capable
@property (NS_NONATOMIC_IOSONLY,                      readonly) float voxelVolume;		// returns the volume of the voxels in units cubed (e.g. mm3)

// -- metrics accessor methods - these methods return 0 if an image has not been read yet with either sliceAsRawData or sliceAsCGImageRef --

@property (NS_NONATOMIC_IOSONLY, readonly)    int                     planes;
@property (NS_NONATOMIC_IOSONLY, readonly)    int                      width;
@property (NS_NONATOMIC_IOSONLY, readonly)    int                     height;
@property (NS_NONATOMIC_IOSONLY, readonly)    int      bitsPerPixelComponent;
@property (NS_NONATOMIC_IOSONLY, readonly)    int                bytesPerRow;
@property (NS_NONATOMIC_IOSONLY, readonly) double         monochromeMinValue;
@property (NS_NONATOMIC_IOSONLY, readonly) double         monochromeMaxValue;
@property (NS_NONATOMIC_IOSONLY, readonly) double monochromeMinPossibleValue;
@property (NS_NONATOMIC_IOSONLY, readonly) double monochromeMaxPossibleValue;

// CGImage Example (Note CGImageRef is read on demand)
//
// iiDcmSlice *slice = [[iiDcmSlice alloc] initWithDicomFileSlice: __DICOM_FILENAME__];
// CGImageRef *image = [slice sliceAsCGImageRef: YES];
//
// -----
//
// NSImage Example (Note NSImage is read on demand)
// you could put this in draw Rect (although you'd want to load the image outside of drawRect)
//
// iiDcmSlice *slice = [[iiDcmSlice alloc] initWithDicomFileSlice: __DICOM_FILENAME__];
//
// NSImage *image = [slice sliceAsNSImage: YES context: nil];
//
// [image setScalesWhenResized:       YES];       // set autoscaling of image - resize to rect & display
// [image              setSize: rect.size];
// [image drawAtPoint: NSMakePoint(0.0, 0.0)
// 		     fromRect: NSMakeRect(0, 0, rect.size.width, rect.size.height)
// 		    operation: NSCompositeSourceOver
// 		     fraction: 1.0];

@end
