/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.
 
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

// -- hack to quiet warning messages in Xcode for now     --
// -- https://www.bignerdranch.com/blog/a-bit-on-warnings --

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#pragma clang diagnostic ignored "-Wconversion"

#import "iiDicom.h"

// -- http://stackoverflow.com/questions/31665095/xcode-error-compiling-c-expected-member-name-or-after-declaration-specifie --
// -- fixed in iiDicom_Prefix.pch                                                                                            --

// #undef verify	// -- AssertMacros.h also defines verify.  Need this to stop tons of compilation errors

#pragma clang diagnostic pop

@implementation iiDcmSeries

#pragma mark - Initializers
#pragma mark -

- (instancetype) init
{
	if (!(self = [super init])) return nil;
	_slices = [[NSMutableArray alloc] initWithCapacity: 1];

	return self;
}

// -- supply an NSArray of NSString filenames to initialize - (reads in dicom on demand) --

- (instancetype) initWithDicomFileSeries: (NSArray *) fileSeries
{
	if (!(self = [super init])) return nil;
	
    _slices = [[NSMutableArray alloc] initWithCapacity: fileSeries.count];
	
        NSString *filename;
	NSEnumerator        *e = [fileSeries objectEnumerator];
    
	while (filename = [e nextObject]) [self addDicomFileToSeries: filename];
		
	return self;
}

#pragma mark - Other Methods
#pragma mark -

// -- adds a dicom filename slice into the series --

- (void) addDicomFileToSeries: (NSString *) filename
{
	iiDcmSlice *slice = [[iiDcmSlice alloc] initWithDicomFileSlice: filename];
	if (slice) [_slices addObject: slice];
}

// -- Sort the slices into order by using the information in the dictionary. --

- (void) sortIntoSliceOrder
{
	
@autoreleasepool {
	
// -- Setup a sort descriptor.  Sort by sliceLocation followed by sliceInstanceNumber --
// -- InstanceNumber can be interleaved on Siemens.  However sliceLocation might not  --
// -- change between slices if for example the heart is looked at through time.       --
// -- So we sort by sliceLocation first, then sliceInstanceNumber.                    --

    NSSortDescriptor       *sliceLocationDescriptor = [[NSSortDescriptor alloc] initWithKey: @"sliceLocation"       ascending: YES];
    NSSortDescriptor *sliceInstanceNumberDescriptor = [[NSSortDescriptor alloc] initWithKey: @"sliceInstanceNumber" ascending: YES];
             NSArray               *sortDescriptors = @[sliceLocationDescriptor, sliceInstanceNumberDescriptor];
             NSArray                     *newSlices = [_slices sortedArrayUsingDescriptors: sortDescriptors];        // sort the array
		
    _slices = (NSMutableArray *) newSlices;		//We don't need slices anymore, release and replace with newSlices
    
}       // autoreleasepool ends here

}

- (NSArray *) slices { return (NSArray *) _slices; }

// -- returns the series as an array of raw data slices - returns nil in case of error --

- (NSArray *) rawDataAs3DVolume
{
    NSData *nd;
    
    if (!_rawDataVolume)
	{
		if ((_rawDataVolume = [[NSMutableArray alloc] initWithCapacity: _slices.count]) == nil)
		{
			DLog (@"iiDcmSeries: rawDataAs3DVolume: alloc failed");
			return nil;
		}
		
		for (int i = 0; i < _slices.count; i++)
		{
			nd = [_slices[(unsigned) i] sliceAsRawData: NO];
            
			if (nd == nil)
			{
				DLog (@"iiDcmSeries: rawDataAs3DVolume: sliceAsRawData failed");
				_rawDataVolume = nil;
				return nil;
			}

            [_rawDataVolume addObject: nd];
		}
	}
	
	return (NSArray *) _rawDataVolume;
}

// -- return the series as an array of raw data slices in scaled units (Hounsfield Units if CT) --
// -- return nil if this isn't a dataset that is monochrome                                     --

- (NSArray *) rawScaledDataAs3DVolume
{
    NSData *nd;
    
    if (!_rawScaledDataVolume)
	{		
		if ((_rawScaledDataVolume = [[NSMutableArray alloc] initWithCapacity:_slices.count])==nil)
		{
			DLog (@"iiDcmSeries: rawScaledDataAs3DVolume: alloc failed");
			return nil;
		}
		
		for (int i = 0; i < _slices.count; i++)
		{
			nd = [_slices[(unsigned) i] sliceAsRawScaledData];

			if (nd == nil)
			{
				DLog (@"iiDcmSeries: rawScaledDataAs3DVolume: sliceAsRawScaledData failed");
				_rawScaledDataVolume = nil;
				return nil;
			}

            [_rawScaledDataVolume addObject: nd];
		}
	}
	
	return (NSArray *) _rawScaledDataVolume;
}

- (NSData *) rawScaledDataAs3DVolumeAsNSData
{
	   NSArray     *volume = [self rawScaledDataAs3DVolume];
	iiDcmSlice *firstSlice = _slices[0];
           int       width = [firstSlice  width];
           int      height = [firstSlice height];

	if (_rawScaledDataVolumeContiguous)	return [NSData dataWithBytesNoCopy: (void *) _rawScaledDataVolumeContiguous
                                                                    length: (unsigned) height * (unsigned) width * (unsigned) volume.count * (unsigned) sizeof(float)
                                                              freeWhenDone: NO];

	if ((_rawScaledDataVolumeContiguous = (float *) malloc((size_t) width * (size_t) height * (size_t) volume.count * sizeof(float))) == NULL)
		then return NULL;
	
	 float  *p = _rawScaledDataVolumeContiguous;
	NSData *nd;
	
	for (int i = 0; i < volume.count; i++)
	{
		nd = volume[(unsigned) i];
		bcopy ((void *) nd.bytes, (void *) p, (size_t) (height * width) * sizeof(float));
		p += height * width;
	}

	return [NSData dataWithBytesNoCopy: (void *) _rawScaledDataVolumeContiguous
								length: (unsigned) height * (unsigned) width * (unsigned) volume.count * (unsigned) sizeof(float)
						  freeWhenDone: NO];
}

#pragma mark - Memory Management
#pragma mark -

- (void) dealloc
{
	DLog (@"iiDcmSeries: dealloc");
	if (_rawScaledDataVolumeContiguous) then free (_rawScaledDataVolumeContiguous);
}

#pragma mark - Deprecated
#pragma mark -

- (NSArray *) hounsfieldAs3DVolume { return [self rawScaledDataAs3DVolume]; }

@end
