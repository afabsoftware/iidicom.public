/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.

 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

@interface iiDcmSeries : NSObject
{
	NSMutableArray                         *_slices;
	NSMutableArray                  *_rawDataVolume;
	NSMutableArray            *_rawScaledDataVolume;
             float	*_rawScaledDataVolumeContiguous;
}

- (instancetype)                    init                         NS_DESIGNATED_INITIALIZER;  // Use this is adding slices one by one with addDicomFileToSeries
- (instancetype) initWithDicomFileSeries: (NSArray *) fileSeries NS_DESIGNATED_INITIALIZER;  // Use this is creating a series with a number of files

- (void) addDicomFileToSeries: (NSString *) filename;		// -- Adds a dicom file to series
- (void)   sortIntoSliceOrder;								// -- Not implemented yet:  Use to sort the series into slice order

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray            *hounsfieldAs3DVolume;	// deprecated - Use rawScaledDataAs3DVolume
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray                          *slices;	// NSArray of iiDcmSlices
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray               *rawDataAs3DVolume;	// series as an array of raw data slices (NSData)
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray         *rawScaledDataAs3DVolume;	// series as an array of raw data slices (NSData) in scaled units (or hounsfield units if CT) (floats)
@property (NS_NONATOMIC_IOSONLY, readonly, copy)  NSData *rawScaledDataAs3DVolumeAsNSData;	// series as a contiguous (NSData) in scaled units (or hounsfield units if CT) (floats) - ordered as slice, row, column

// iiDcmSeries    *series = [[iiDcmSeries alloc] init];
//     NSArray *rawVolume;
//
// [series addDicomFileToSeries: @"__DICOM_FILENAME_1__"];
// [series addDicomFileToSeries: @"__DICOM_FILENAME_2__"];
// [series addDicomFileToSeries: @"__DICOM_FILENAME_3__"];
//
// [series sortIntoSliceOrder];
//
// iiDcmSlice *slice = [[series slices] objectAtIndex: 1];
// if ((rawVolume = [series rawDataAs3Dvolume]) == nil)	DLog (@"rawDataAs3DVolume = nil");
//
// DLog (@"Width(%d) Height(%d) Planes(%d)", [slice width], [slice height], [slice planes]);        // do something with rawVolume

@end
