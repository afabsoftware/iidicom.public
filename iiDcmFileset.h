/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.
 
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

@interface iiDcmFileset : NSObject
{
	NSArray *_dicomSeries;		// array of iiDcmSeries
}

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *series;       // an array of series

// -- overide these methods to create a progress bar - see iiDcmFileset.m for more documentation --

- (instancetype) init;                                                                  // DO NOT USE
- (instancetype) initWithDicomFileset: (NSArray *) fileset NS_DESIGNATED_INITIALIZER;	// use this to instantiate

- (void)            progressStart;
- (void)          progressSorting;
- (void)              progressEnd;
- (void)   progressSetDeterminate;
- (void) progressSetIndeterminate;

- (BOOL) progressUpdate: (double) amount filename: (NSString *) filename;

//  -- example (Note 3d volume is read on demand) --
//
//      NSArray *filenames = [NSArray arrayWithObject: @"__DICOM_FOLDER(S)__"];
// iiDcmFileset   *fileset = [[iiDcmFileset alloc] initWithDicomFileset: filenames];
//  iiDcmSeries    *series = [[fileset series] objectAtIndex: 0];
//      NSArray *rawVolume;
//
//  if ((rawVolume = [series rawDataAs3Dvolume]) == nil) DLog (@"rawDataAs3DVolume = nil");
// 
//  iiDcmSlice *slice = [[series slices] objectAtIndex: 0];
//  DLog (@"Width(%d) Height(%d) Planes(%d)", [slice width], [slice height], [slice planes]);
//  [fileset release];           // not required under ARC

@end
