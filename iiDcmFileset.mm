/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved. 
 
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

// -- hack to quiet warning messages in Xcode for now     --
// -- https://www.bignerdranch.com/blog/a-bit-on-warnings --

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#pragma clang diagnostic ignored "-Wconversion"

#import "iiDicom.h"

// -- http://stackoverflow.com/questions/31665095/xcode-error-compiling-c-expected-member-name-or-after-declaration-specifie --
// -- fixed in iiDicom_Prefix.pch                                                                                            --

// #undef verify	// -- AssertMacros.h also defines verify.  Need this to stop tons of compilation errors

#pragma clang diagnostic pop

@implementation iiDcmFileset

#pragma mark - Initializers
#pragma mark -

- (instancetype) init
{
	DLog (@"iiDcmFileset: Use initWithDicomFileset instead of init to instantiate object");
	return [self initWithDicomFileset: nil];
}

// -- Supply an NSArray of NSString filenames/directories, to initialize. --

- (instancetype) initWithDicomFileset: (NSArray *) fileset
{
            int multipleSeriesIndex;
    iiDcmSeries             *series;
	
	if (!(self = [super init])) return nil;
	
	if (!fileset) return nil;
	
	[self            progressStart];
	[self progressSetIndeterminate];
    	
@autoreleasepool {
    
	NSArray *expandedFileset = [self expandFileset: fileset];
	
// -- If the expandedFileset is empty, do nothing and return. --

    if (expandedFileset.count == 0)
	{
		[self progressEnd];
		return nil;
	}

	[self progressSetDeterminate];

	double      progress = (double) 0;
	double progressTotal = (double) expandedFileset.count;

// -- Loop through the files in the expandedFileset, comparing to the first slice in each series. --
// -- If not found, create a new series and add it. - If found, add slice to the existing series. --
    
	NSMutableArray *multipleSeries = [[NSMutableArray alloc] initWithCapacity: 1];
      NSEnumerator              *e = [expandedFileset objectEnumerator];
          NSString       *filename;
    
	while (filename = [e nextObject])
	{
		if ([self progressUpdate: (progress / progressTotal) * (double) 100.0 filename: filename.lastPathComponent])
		{
			[self progressEnd];
			return nil;
		}
		
		multipleSeriesIndex = [self sliceByFilenameMatchesSeries: filename multipleSeries: multipleSeries];

        if (multipleSeriesIndex == -1)
		{
			iiDcmSeries *series = [[iiDcmSeries alloc] init];
            
			[series addDicomFileToSeries: filename];
			[multipleSeries addObject: series];
		}
		else
		{
			iiDcmSeries *series = multipleSeries[multipleSeriesIndex];
            
			[series addDicomFileToSeries: filename];
		}
		
		progress = progress + (double) 1.0;
	}
	
	[self progressSetIndeterminate];
	[self          progressSorting];
	
// -- now sort the series into slice order --
	
	e = [multipleSeries objectEnumerator];
	while (series = [e nextObject])	[series sortIntoSliceOrder];
		
	_dicomSeries = (NSArray *) multipleSeries;
	
// -- added (unsigned long) casts to update code to ARC --
    
    DLog (@"Fileset Series Deduced: %lu", (unsigned long) _dicomSeries.count);
	int seriesCount = 0;

	e = [_dicomSeries objectEnumerator];
	while (series = [e nextObject])
	{
		DLog (@"Series[%d] %lu items", seriesCount, (unsigned long) [series slices].count);
		if ([self containsMoreThanOneSeries: series])
			DLog (@"This series contains duplicate InstanceNumbers, likely the DICOM dictionary is corrupt and not providing a unique seriesUid!");
		seriesCount++;
	}	
	
	[self progressEnd];
	
//  for (iiDcmSeries *series in _dicomSeries)
//  {
//      DLog (@"New Series");
//      for (iiDcmSlice *slice in [series slices])
//      {
//          DLog (@"Slice Number: %@  seriesUid: %@  Filename: %@", [slice sliceLocation], [[slice dicomDictionary] seriesUid], [slice filename]);
//      }
//  }
    
}       // autoreleasepool ends here
	
	return self;
}

#pragma mark - Other Methods
#pragma mark -

- (int) sliceByFilenameMatchesSeries: (NSString *) filename multipleSeries: (NSArray *) multipleSeries
{
	if (multipleSeries.count == 0) return -1;
	
	iiDcmSeries *series;
    
             int seriesCount = 0;
            BOOL       match = NO;
	NSEnumerator          *e = [multipleSeries objectEnumerator];
    
	while (series = [e nextObject])
	{
		
@autoreleasepool {
    
        iiDcmDictionary *compareToDict = [[iiDcmDictionary alloc] initWithDicomFile: filename];
			
        if ([[[[series slices][0] dicomDictionary] seriesUid] isEqualToString: [compareToDict seriesUid]]) then match = YES;
			
        DLog (@"sliceByFilenameMatchesSeries: seriesCount: %d", seriesCount);
        if (match) then return seriesCount;

}       // autoreleasepool ends here
		
		seriesCount++;
	}
    
	return -1;		
}

// -- expands all directories in a fileset into files --

- (NSArray *) expandFileset: (NSArray *) fileset
{
        BOOL  directory;
        BOOL directory2;
    NSString  *filename;

    NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity: fileset.count];
	  NSEnumerator   *e = [fileset objectEnumerator];
    
	while (filename = [e nextObject])
	{
		[[NSFileManager defaultManager] fileExistsAtPath: filename isDirectory: &directory];

		if (directory)
		{
			NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath: filename];
                         NSString    *subPath;
            
			while (subPath = [enumerator nextObject])
			{
				// -- only add files that end with .dcm extension --
                
				NSString *path = [[NSString alloc] initWithFormat: @"%@/%@", filename, subPath];
                
				[[NSFileManager defaultManager] fileExistsAtPath: path isDirectory: &directory2];
				if (([path.pathExtension isEqualToString: @"dcm"]) && (!directory2))
                    then [ret addObject: path];
			}
		}
		else	
			{
				// -- only add files ending with .dcm extension --
                
				if ([filename.pathExtension isEqualToString: @"dcm"]) [ret addObject: filename];
			}
	}
	
	DLog (@"Expanded List Of Files: %@", ret);
	return (NSArray *) ret;
}

//  -- Returns YES if the first 2 items in the series have the same sliceLocation. --

- (BOOL) containsMoreThanOneSeries: (iiDcmSeries *) series
{

    if ( [series slices].count <= (int) 1) return NO;       	// need at least 2 entries for this test to be valid
	
@autoreleasepool {

		NSNumber *sliceOneLocation = [[series slices][0]       sliceLocation];
		NSNumber *sliceTwoLocation = [[series slices][1]       sliceLocation];
		NSNumber *sliceOneInstance = [[series slices][0] sliceInstanceNumber];
		NSNumber *sliceTwoInstance = [[series slices][1] sliceInstanceNumber];
		
		BOOL ret = NO;
		
		if (([sliceOneLocation compare: sliceTwoLocation] == NSOrderedSame) &&
			([sliceOneInstance compare: sliceTwoInstance] == NSOrderedSame))
		{
			DLog (@"Duplicate slice in series: %@ %@", sliceOneInstance, sliceOneLocation);
			ret = YES;
		}
		
		return ret;
    
}       // autoreleasepool ends here

}

- (NSArray *) series { return (NSArray *) _dicomSeries; }

// you can overide these methods to create a progress bar
// The typical call order is:
//
//		progressStart
//		progressSetIndeterminate
//		progressSetDeterminate
//		progressUpdate (for each file in the fileset)
//		progressSetIndeterminate
//		progressSorting
//		progressEnd

- (void)            progressStart { }   // called when a progress bar needs to be created
- (void)              progressEnd { }   // called when a progress bar is no longer needed
- (void)          progressSorting { }   // called when the files have been read in and series start to be sorted into slice order
- (void)   progressSetDeterminate { }   // called when a progress bar is   determinate, i.e. when the amount of progress is   known (solid  blue bar)
- (void) progressSetIndeterminate { }   // called when a progress bar is indeterminate, i.e. when the amount of progress is unknown (candy store bar)

//Returns YES if the user cancelled the progress bar otherwise NO
//Amount is the percentage (0-100) that should be displayed on the bar.  Filename is the current file that is being processed

- (BOOL) progressUpdate: (double) amount filename: (NSString *) filename { return NO; }

#pragma mark - Memory Management
#pragma mark -

- (void) dealloc
{
	DLog (@"iiDcmFileset: dealloc");

//	iiDcmSeries *series;
//	NSEnumerator *e = [_dicomSeries objectEnumerator];
//	while (series = [e nextObject]);                        // Did the release get removed due to ARC conversion?
}

@end
