/* Copyright � 2007, Imaging Informatics - All rights reserved.
   Copyright � 2016, additional modifications by AFAB Software - All rights reserved.

 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 
 *	Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer.
 
 *	Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation 
 and/or other materials provided with the distribution.
 
 *	Neither the name of the Imaging Informatics Group, Calgary, Alberta, Canada
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

// -- hack to quiet warning messages in Xcode for now     --
// -- https://www.bignerdranch.com/blog/a-bit-on-warnings --

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#pragma clang diagnostic ignored "-Wconversion"

#import "iiDicom.h"

// -- http://stackoverflow.com/questions/31665095/xcode-error-compiling-c-expected-member-name-or-after-declaration-specifie --
// -- fixed in iiDicom_Prefix.pch                                                                                            --

// #undef verify	// -- AssertMacros.h also defines verify.  Need this to stop tons of compilation errors

#pragma clang diagnostic pop

@implementation iiDcmDictionary

#pragma mark - Initializers
#pragma mark -

// -- provided in case initWithDicomFile isn't called instead --

- (instancetype) init
{
	DLog (@"iiDcmDictionary: Use initWithDicomFile instead of init to instantiate object");
	return [self initWithDicomFile: @"unknown.dcm"];
}

// -- registers the filename provided - stored in _dcmFilename --

- (instancetype) initWithDicomFile: (NSString *) dicomFilename
{
	if (!(self = [super init])) return nil;
	
	_dcmFilename = dicomFilename;	
	    fileRead = NO;
	
	return self;
}

#pragma mark - Other Methods
#pragma mark -

- (void) readDicomDictionaryFromFile: (NSMutableDictionary *) dict dataset: (DcmDataset *) dset
{

       DcmStack      stack;
     DcmElement     *delem = NULL;
      DcmObject   *dobject = NULL;
	OFCondition statusDset = dset->nextObject(stack, OFTrue);
            int      count = 0;
	
@autoreleasepool {
	
		while (statusDset.good( ))
		{
			dobject = stack.top( );
			  delem = (DcmElement *) dobject;
			
			DcmVR dvr (dobject->getVR( ));
			
//-- get the tag (key) --
            
			  DcmTag   dtag = dobject->getTag( );
			NSString   *key = @(dtag.getTagName( ));
            NSString *value = @"";                      // no value or the value is NULL
            
              char *psz; Uint16 us;  Sint16 ss;   Uint8 *uia;
            Uint32   ul; Sint32 sl; Float32 sf; Float64   lf;
			
			if (delem->valueLoaded( )) {
				const char *vr = dvr.getVRName( );
				if ((strcmp(vr, "AE") == 0) || (strcmp(vr, "AS") == 0) || (strcmp(vr, "CS") == 0) || (strcmp(vr, "DA") == 0) || (strcmp(vr, "DS") == 0) ||
                    (strcmp(vr, "DT") == 0) || (strcmp(vr, "IS") == 0) || (strcmp(vr, "LO") == 0) || (strcmp(vr, "LT") == 0) || (strcmp(vr, "PN") == 0) ||
                    (strcmp(vr, "SH") == 0) || (strcmp(vr, "ST") == 0) || (strcmp(vr, "TM") == 0) || (strcmp(vr, "UN") == 0) || (strcmp(vr, "UT") == 0)) {

                    delem->getString(psz);
					if (psz) value = @(psz);
				}
				else if ((strcmp(vr, "US") == 0) || ((strcmp(vr, "xs") == 0) && (dvr.getValidEVR( ) == EVR_US))) {
                    
					delem->getUint16(us);
					value = [NSString stringWithFormat: @"%hu", us];
				}
				else if ((strcmp(vr, "SS") == 0) || ((strcmp(vr, "xs") == 0) && (dvr.getValidEVR( ) == EVR_SS))) {
                    
					delem->getSint16(ss);
					value = [NSString stringWithFormat: @"%hi", ss];
				}
				else if (strcmp(vr, "UL") == 0) {
                    
					delem->getUint32(ul);
					value = [NSString stringWithFormat: @"%u", ul];
				}
				else if (strcmp(vr, "SL") == 0) {
                    
					delem->getSint32(sl);
					value = [NSString stringWithFormat: @"%i", sl];
				}
				else if (strcmp(vr, "FL") == 0) {
                    
					delem->getFloat32(sf);
					value = [NSString stringWithFormat: @"%f", (double) sf];
				}
				else if (strcmp(vr, "FD") == 0) {
                    
					delem->getFloat64(lf);
					value = [NSString stringWithFormat: @"%f", lf];
				}
				else if (strcmp(vr, "OB") == 0) {
                    
					delem->getUint8Array(uia);
					
					for (int i = 0; i < delem->getLength(EXS_LittleEndianImplicit, EET_UndefinedLength); i++)
					{
						if (isprint((int) uia[i]))
                            then value = [value stringByAppendingFormat:        @"%c",          (char) uia[i]];
                            else value = [value stringByAppendingFormat: @"(%%%02lX)", (unsigned long) uia[i]];
					}
				}
				else if (strcmp(vr, "UI") == 0) {
					
// -- convert UID into name if possible - otherwise default to UID --
                    
                    delem->getString(psz);
					if (psz) {
						const char *uid = dcmFindNameOfUID(psz);
						if (uid)	value = @(uid);
						else		value = @(psz);
					}
				}					
				
// -- These VR's probably need to be implemented too:  AT OW SQ. --
				
                else value = [NSString stringWithFormat: @"Unknown Tag (%s)", vr];				// undefined tags are noted here

// -- add to dictionary - If Object already exists, tag on "Duplicate <count>". --
				
                if (dict[key])
				{
					key = [key stringByAppendingFormat: @"Duplicate %04d", count];
				}
				dict[key] = value;
				
				count++;
			}
			
			statusDset = dset->nextObject(stack, OFTrue);
		}
	
}       // autoreleasepool ends here

}

// -- Read the Dicom Dictionaries in.  Return YES if the dicom file cannot be read, otherwise NO. --
// -- only reads the dictionary if it hasn't been read in already                                 --

- (BOOL) readDicomDictionary
{
	if (!fileRead)
	{
        DcmFileFormat dfile;
        
                fileRead = YES;
		    _metaDataSet = [[NSMutableDictionary alloc] init];
		_metaInformation = [[NSMutableDictionary alloc] init];
	
// -- load in the dicom file and grab the 2 dictionaries --

@autoreleasepool {

        OFCondition status = dfile.loadFile(_dcmFilename.UTF8String);
		
		if (status.good( ))
		{
			DcmDataset *dset;
            
			dset = dfile.getDataset( );
			[self readDicomDictionaryFromFile: _metaDataSet dataset: dset];
			
			dset = (DcmDataset *) dfile.getMetaInfo( );
			[self readDicomDictionaryFromFile: _metaInformation dataset: dset];
		}
		else
		{
			DLog (@"iiDcmDictionary: Unable to read DICOM file: %@ (%s)", _dcmFilename, status.text( ));
			return YES;
		}
	}

}       // autoreleasepool ends here
	
	return NO;
}

// -- returns the Meta Information dictionary from the dicom file - nil if the dicom file can't be opened --

- (NSDictionary *) metaInformation
{
	if ([self readDicomDictionary])
        then return nil;
        else return (NSDictionary *) _metaInformation;
}

// -- returns the DataSet dictionary from the dicom file - nil if the dicom file can't be opened --

- (NSDictionary *) metaDataSet
{
	if ([self readDicomDictionary])
        then return nil;
        else return (NSDictionary *) _metaDataSet;
}

// -- returns the value indexed by key in the dataset dictionary - nil if there is no entry --

- (NSString *) metaDataSetValueByKey: (NSString *) key
{
	if ([self readDicomDictionary])
        then return nil;
        else return _metaDataSet[key];
}

// -- returns the value indexed by key in the information dictionary - nil if there is no entry --

- (NSString *) metaInformationValueByKey: (NSString *) key
{
	if ([self readDicomDictionary])
        then return nil;
        else return _metaInformation[key];
}

// -- puts the key value in the ImageKit Properties Dictionary only if the key exists --

- (void) createKeyValueInMetaIK: (NSString *) key
{
	NSString *value = [self metaDataSetValueByKey: key];
    
	if (value)
        then _metaForImageKitProperties[key] = value;
}

// -- gets both meta dictionaries and returns then in "Tabbed" windows suitable for display in Image Kit "Image Edit" Panel --

- (NSDictionary *) metaDictionaryAllForImageKitProperties
{
	if ([self readDicomDictionary])	return nil;
	else
	{
		if (!_metaForImageKitProperties)
		{
			_metaForImageKitProperties = [[NSMutableDictionary alloc] init];

			_metaForImageKitProperties[@"Filename"] = _dcmFilename;         // general tab

            [self createKeyValueInMetaIK:          @"Columns"];
			[self createKeyValueInMetaIK:             @"Rows"];
			[self createKeyValueInMetaIK:       @"BitsStored"];
			[self createKeyValueInMetaIK:     @"PatientsName"];
			[self createKeyValueInMetaIK:      @"ContentDate"];
			[self createKeyValueInMetaIK:         @"Modality"];
			[self createKeyValueInMetaIK:     @"PixelSpacing"];
			[self createKeyValueInMetaIK:   @"SliceThickness"];
			[self createKeyValueInMetaIK: @"StudyDescription"];

			_metaForImageKitProperties[   @"{Meta}"] = (NSDictionary *) _metaInformation;
			_metaForImageKitProperties[@"{Dataset}"] = (NSDictionary *) _metaDataSet;
		}

		return (NSDictionary *) _metaForImageKitProperties;
	}
}

// -- returns the dicom filename --

- (NSString *) dicomFilename { return _dcmFilename; }

// -- gets the given key from the dictionary - returns an empty string if the key doesn't exist --

- (NSString *) _metaDataSetValueByKeyOrEmpty: (NSString *) key
{
	NSString *ret = [self metaDataSetValueByKey: key];
	
	if (!ret)
        then return @"";
        else return ret;
}

// -- returns a unique identifier, which uniquely identifies the series this slice belongs to --

- (NSString *) seriesUid
{
	NSString *uid = [[NSString alloc] initWithFormat: @"%@#%@#%@#%@#%@#%@#%@#%@#%@#%@#%@",
						[self _metaDataSetValueByKeyOrEmpty:      @"PatientsName"],
						[self _metaDataSetValueByKeyOrEmpty:         @"PatientID"],
						[self _metaDataSetValueByKeyOrEmpty:   @"AccessionNumber"],
						[self _metaDataSetValueByKeyOrEmpty:         @"StudyDate"],
						[self _metaDataSetValueByKeyOrEmpty:          @"Modality"],
						[self _metaDataSetValueByKeyOrEmpty:        @"SeriesDate"],
						[self _metaDataSetValueByKeyOrEmpty: @"SeriesDescription"],
						[self _metaDataSetValueByKeyOrEmpty: @"SeriesInstanceUID"],
						[self _metaDataSetValueByKeyOrEmpty:      @"SeriesNumber"],
						[self _metaDataSetValueByKeyOrEmpty:        @"SeriesTime"],
						[self _metaDataSetValueByKeyOrEmpty:          @"EchoTime"]];

    return uid;
}

// -- swift bridging forces us to use an array to get the tag IDs this way --

/*
You need to change the black pixels to transparent.  Which means setting the alpha channel to 0 or something less than 255.  Dicom images are not stored with an alpha channel.  You will need to copy the image from whatever array type it's in, to a 32 bit per pixel array. Then set the alpha channel.   You need to retrieve the samples per pixel tag (0028,0002) to know the bytes per pixel (it will be either 1 or 3).  You should be doing this now to know how big an array to allocate before getting the pixel data from the dicom file.  Then allocate a 4 byte array to hold each pixel.  Copy the pixels, then set the alpha channel to the value you want. (I should have said set the black pixels to transparent.)
 
Yeah.  Bad dicom programming.  They're assuming they know the image format.  It also looks like they're assuming the planar configuration (tag (0028,0006), which has a value of 0 or 1 ). Color by pixel (value = 0) means the pixel byte order is RGBRGBRGB...  Color by plane (value - = 1) means the first plane is RRRRRRRRR, then GGGGGGGGG,  then BBBBBBB for the third.   You'll need to check it to determine how to access a single pixel to see if it's black.
 
Also look at tag (0028,0008), Number of Frames in a multi-frame image.  If this tag is missing, then there's only 1 image in the file.  Or it could be set to 1.  The tag is required if there's more than one frame in the file.
 
In dcmtk there are a large number of methods of the form findAndGetXXX where XXX is a type, e.g. findAndGetUInt16(tag, value).  These methods are defined for datasets and items.  You'll need to know the type used to store the data.  This can be found in the dicom documentation.  Or you can do what I do, which is google "dcmtk DCM_NumberOfFrames".  DCM_NumberOfFrames is the #define for the tag (0028,0008).  It can be found in the file dcdeftag.h.  For DCM_NumberOfFrames I found an example that used findAndGetSInt32( ).  dcmtk does a good job of type conversion.  So you can guess that DCM_NumberOfFrames would be an integer and it would work.  If you look in the dcmtk source code for the findAndGet... routines it tells you what type conversions that routine will do.
*/

#pragma mark - Methods For Use With Swift
#pragma mark -

// -- experimental - rework fileformat.loadFile calls to reduce overhead --
// -- derived from latest_dcmtk/dcmdata/libsrc/dcitem.cc                 --
// --                                                                    --
// -- alternatives for obtaining certain DCMTK information               --
// --                                                                    --
// -- Uint32              j = dcmImage->getFrameCount( );                --
// -- Uint32              k = dcmImage->getNumberOfFrames( );            --
// --    int _bitsPerSample = dcmImage->getDepth( );                     --

- (BOOL) tagExistsWithValue: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

    DcmFileFormat fileformat;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->tagExistsWithValue(DcmTagKey(group, element)))
        then return YES;
        else return  NO;

}

- (BOOL) tagExists: (NSArray *) tagPair
{
    
    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];
    
    DcmFileFormat fileformat;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->tagExists(DcmTagKey(group, element)))
        then return YES;
        else return  NO;
    
}

- (NSNumber *) findAndGetUint16: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

    DcmFileFormat  fileformat;
           UInt16 returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetUint16(DcmTagKey(group, element), returnValue).good( ))
        then return [[NSNumber alloc] initWithUnsignedInt: (unsigned int) returnValue];
        else return nil;
    
}

- (NSNumber *) findAndGetSint32: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

    DcmFileFormat  fileformat;
           SInt32 returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetSint32(DcmTagKey(group, element), returnValue).good( ))
        then return [[NSNumber alloc] initWithInt: (int) returnValue];
        else return nil;
    
}

- (NSString *) findAndGetOFString: (NSArray *) tagPair
{
    
    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];
    
    DcmFileFormat  fileformat;
         OFString returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetOFString(DcmTagKey(group, element), returnValue).good( ))
        then return [NSString stringWithCString: returnValue.c_str( ) encoding: [NSString defaultCStringEncoding]];
        else return nil;
    
}

- (NSNumber *) findAndGetFloat32: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

    DcmFileFormat  fileformat;
            float returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetFloat32(DcmTagKey(group, element), returnValue).good( ))
        then return [[NSNumber alloc] initWithFloat: returnValue];
        else return nil;
    
}

- (NSNumber *) findAndGetFloat64: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

    DcmFileFormat  fileformat;
           double returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetFloat64(DcmTagKey(group, element), returnValue).good( ))
        then return [[NSNumber alloc] initWithDouble: returnValue];
        else return nil;
    
}

/*
 - (NSMutableArray *) findAndGetFloat32Array: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

          DcmFileFormat     fileformat;
    const       Float32   *returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    if (fileformat.getDataset( )->findAndGetFloat32Array(DcmTagKey(group, element), returnValue).good( ))
        then { DLog (@"-- got values for findAndGetFloat32Array --"); }
        else { DLog (@"--  no values for findAndGetFloat32Array --"); }
    
    return nil;

}

 - (NSMutableArray *) findAndGetFloat64Array: (NSArray *) tagPair
{

    UInt16   group = (UInt16) [[tagPair objectAtIndex: 0] unsignedIntegerValue];
    UInt16 element = (UInt16) [[tagPair objectAtIndex: 1] unsignedIntegerValue];

          DcmFileFormat     fileformat;
    const       Float64   *returnValue;
    
    fileformat.loadFile (_dcmFilename.UTF8String);
    
    OFCondition status = fileformat.getDataset( )->findAndGetFloat64Array(DcmTagKey(group, element), returnValue);

    return nil;

}
*/

@end
